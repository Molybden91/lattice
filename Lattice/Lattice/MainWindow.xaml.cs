﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Lattice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region private variables
        private int PointsForOneGraph;
        private int numberOfSeries;
        private double offset;
        private List<PlotModel> plotModels;
        private List <int> ObtainedPoints;
        #endregion

        #region public variables

        public PlotModel PlotModelForGeneralPicture{ get; set; }
        
        #endregion

        #region Constructors
        public MainWindow()
        {
            PlotModelForGeneralPicture = new PlotModel ();
            LinearAxis linearAxisOY = new LinearAxis();
            linearAxisOY.Position = AxisPosition.Left;

            PlotModelForGeneralPicture.Axes.Add(linearAxisOY);

            InitializeComponent();
        }
        #endregion

        #region Button Actions
        private void ButtonFile_Click(object sender, EventArgs e) {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = "c:\\Users\\am\\Documents\\АнтеннаяРешетка\\TestData\\",
                Filter = "data files v3 (*.raw)|*.raw|data files v4 (*.is4)|*.is4|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };
            openFileDialog.ShowDialog();
            FileTextBox.Text= openFileDialog.FileName;
            DisplayImageFromFile(openFileDialog.FileName);
        }
        private void ButtonWriteFile_Click(object sender, EventArgs e) {
            Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog ();
            if (dialog.ShowDialog() == true)
            {
                try
                {
                    FileStream fileStream = new FileStream(dialog.FileName, FileMode.CreateNew, FileAccess.Write);
                    StreamWriter fileWriter = new StreamWriter(fileStream);
                    for (int i=0; i < ObtainedPoints.Count; i++) {
                        fileWriter.WriteLine(ObtainedPoints[i]);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error",
                           MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion
   
        #region Auxiliary methods
        private void DisplayImageFromFile(string fileName) {
            try
            {
                PlotModelForGeneralPicture.Series.Clear();
                LineSeries lineSeriesTmp = new LineSeries
                {
                    StrokeThickness = 0,
                    MarkerSize = 1,
                    MarkerStroke = OxyColors.ForestGreen,
                    MarkerType = MarkerType.Cross
                };

                System.IO.BinaryReader br = new System.IO.BinaryReader(File.Open(fileName, FileMode.Open));

                long TotalSize = br.BaseStream.Length;
                int NumberOfPixels = (int)(TotalSize / 2);
                ObtainedPoints = new List<int>();
                for (int i = 0; i < NumberOfPixels; ++i)
                {
                    ObtainedPoints.Add(br.ReadInt16());                  
                }
                if (NumberOfPixels > 0 && NumberOfPixels < 70000)
                {
                    this.PointsForOneGraph = 1638;
                    this.numberOfSeries = 4;
                }
                else if (NumberOfPixels > 100000 && NumberOfPixels < 300000)
                {
                    this.PointsForOneGraph = 4096;
                    this.numberOfSeries = 64;
                }
                else if (NumberOfPixels > 500000)
                {
                    this.PointsForOneGraph = 4096;
                    this.numberOfSeries = 128;
                }
                else {
                    throw new Exception("Not recognized scanner version");
                }
                this.offset = ObtainedPoints.Max()*1.005;
                for (int i = 1; i < this.numberOfSeries+1; i++)
                    {
                        displaySeriesForGeneralPicture(i, ObtainedPoints);
                    }
                br.Close();
                    
                DataContext = this;
                PlotViewForInputFile.InvalidatePlot(true);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error",
                       MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void displaySeriesForGeneralPicture(int seriesNumber, List<int> data) {
            LineSeries lineSeriesTmp = new LineSeries
            {
                StrokeThickness = 0,
                MarkerSize = 1,
                MarkerStroke = OxyColors.ForestGreen,
                MarkerType = MarkerType.Cross
            };
            double currentOffset = 0;
            currentOffset = this.offset * (this.numberOfSeries - seriesNumber);
            int j = 1;
            for (int i = (seriesNumber - 1) * PointsForOneGraph; i < seriesNumber * PointsForOneGraph; i++)
            {
                data[i] = data[i] + (int)currentOffset;
                lineSeriesTmp.Points.Add(new DataPoint(j, data[i]));
                j++;
            }
            currentOffset = 0;
            PlotModelForGeneralPicture.Series.Add(lineSeriesTmp);
        }
        #endregion

    }
}

